# React Native Demos

This project contains some demos which clone airbnb UI

**CREDIT**: [Tutorial: AirBNB UI Clone](https://blog.expo.io/tutorial-airbnb-ui-clone-8bfce9551ec1)

## Initial setup

1. The React Native setup based on the [Getting Started](https://facebook.github.io/react-native/docs/getting-started.html) at `Building Projects with Native Code`.
    
2. Checkout repository
```
 git clone https://gitlab.com/nvg-react-native/Layout-Tutorial-Series.git
```

3. Go to a demo, eg. `TabNavigatorAndHeader`
```
    cd TabNavigatorAndHeader
```   

3. Install all dependencies
```
    npm install
```    

4. Run apps
```
    react-native run-ios
    react-native run-android
```

5. Run app with XCode or Android Studio --- you don't need to do anything, click "run" and see the magic 😌

## Contributing

Contributions are very welcome 🎉 😌